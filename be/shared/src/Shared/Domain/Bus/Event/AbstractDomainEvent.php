<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Event;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;
use RuntimeException;

abstract class AbstractDomainEvent implements DomainEvent
{
    private $eventId;
    private $occurredOn;

    protected function __construct(
        private readonly string $aggregateId,
        private readonly array $data = [],
        ?DateTimeImmutable $occurredOn = null,
        ?string $eventId = null
    ) {
        $this->occurredOn = $occurredOn ?? new DateTimeImmutable();
        $this->eventId = $eventId ?? Uuid::uuid4()->toString();
    }

    public function __call($method, $args)
    {
        $attributeName = $method;
        if (str_starts_with($method, 'is')) {
            $attributeName = lcfirst(substr($method, 2));
        }

        if (str_starts_with($method, 'has')) {
            $attributeName = lcfirst(substr($method, 3));
        }

        if (str_starts_with($method, 'get')) {
            $attributeName = lcfirst(substr($method, 3));
        }

        if (isset($this->data[$attributeName])) {
            return $this->data[$attributeName];
        }

        throw new RuntimeException(sprintf('The method "%s" does not exist.', $method));
    }

    abstract public static function eventName(): string;

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function data(): array
    {
        return $this->data;
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
