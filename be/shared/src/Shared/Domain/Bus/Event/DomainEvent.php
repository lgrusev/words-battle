<?php

namespace Leonix\Shared\Domain\Bus\Event;

use DateTimeImmutable;

interface DomainEvent
{
    /**
     * Retrieve unique domain name
     */
    public static function eventName(): string;

    /**
     * Retrieve unique domain ID
     */
    public function eventId(): string;

    public function occurredOn(): DateTimeImmutable;
}
