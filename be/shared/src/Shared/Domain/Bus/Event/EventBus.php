<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Event;

interface EventBus
{
    public function notify(DomainEvent $event, array $context = []): void;
}
