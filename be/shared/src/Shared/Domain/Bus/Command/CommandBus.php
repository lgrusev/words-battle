<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Command;

interface CommandBus
{
    public function dispatch(Command $command, array $context = []): void;
}
