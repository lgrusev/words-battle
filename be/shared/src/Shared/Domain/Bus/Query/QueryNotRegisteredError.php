<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Query;

use RuntimeException;

final class QueryNotRegisteredError extends RuntimeException
{
    public function __construct(Query $event)
    {
        $queryClass = $event::class;

        parent::__construct("The query <$queryClass> hasn't a query handler associated");
    }
}
