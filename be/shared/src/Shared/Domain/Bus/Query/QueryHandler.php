<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Query;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

interface QueryHandler extends MessageHandlerInterface
{
}
