<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Query;

interface QueryBus
{
    public function ask(Query $query, array $context = []): ?Response;
}
