<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Model;

use JsonSerializable;

final class Error implements JsonSerializable
{
    public function __construct(private readonly string $message, private readonly string $code, private readonly array $data = [])
    {
    }

    public function code(): string
    {
        return $this->code;
    }

    public function message(): string
    {
        return $this->message;
    }

    public function data(): array
    {
        return $this->data;
    }

    public function jsonSerialize(): array
    {
        return [
            'code' => $this->code,
            'message' => $this->message,
            'data' => $this->data,
        ];
    }
}
