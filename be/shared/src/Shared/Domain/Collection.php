<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;
use Webmozart\Assert\Assert;

abstract class Collection implements Countable, IteratorAggregate
{
    private readonly array $items;

    public function __construct(array $items)
    {
        Assert::allIsInstanceOf($items, $this->type());
        $this->items = $items;
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->items());
    }

    public function count(): int
    {
        return count($this->items());
    }

    abstract protected function type(): string;

    protected function items(): array
    {
        return $this->items;
    }

    protected function each(callable $fn)
    {
        foreach ($this->items as $key => $value) {
            $fn($value, $key);
        }
    }
}
