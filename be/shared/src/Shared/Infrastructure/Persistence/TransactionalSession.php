<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Persistence;

use Leonix\Shared\Domain\Bus\Event\DomainEvent;

interface TransactionalSession
{
    public function commit(DomainEvent $afterCommitEvent = null): void;

    public function commitTransactional(callable $operation, DomainEvent $afterCommitEvent = null);
}
