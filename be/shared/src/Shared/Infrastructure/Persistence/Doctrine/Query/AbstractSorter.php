<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Persistence\Doctrine\Query;

use Leonix\Shared\Application\Query\OrderByColumn;

abstract class AbstractSorter
{
    protected const SORTABLE_FIELDS = [];

    /**
     * @param OrderByColumn[] $fieldsList
     * @return OrderByColumn[]
     */
    public static function sort(string $alias, array $fieldsList): array
    {
        if ($fieldsList === []) {
            return [];
        }

        $normalized = [];
        foreach ($fieldsList as $column) {
            if (!isset(static::SORTABLE_FIELDS[$column->name()])) {
                continue;
            }

            $normalized[] = new OrderByColumn(
                str_replace('%alias%', $alias, static::SORTABLE_FIELDS[$column->name()]),
                $column->isDescendant()
            );
        }

        return $normalized;
    }
}
