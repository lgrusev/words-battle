<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Persistence\Doctrine\Query;

abstract class AbstractSelector
{
    protected const FIELD_SELECTOR = [];

    public static function selectAll(string $alias): array
    {
        return self::select($alias, array_keys(static::FIELD_SELECTOR));
    }

    public static function select(string $alias, array $fieldsList = []): array
    {
        if ($fieldsList === []) {
            return array_values(str_replace('%alias%', $alias, static::normalizeSelector(static::FIELD_SELECTOR)));
        }

        return array_values(
            str_replace(
                '%alias%',
                $alias,
                static::normalizeSelector(
                    array_intersect_key(
                        static::FIELD_SELECTOR,
                        array_combine($fieldsList, $fieldsList)
                    )
                )
            )
        );
    }

    private static function normalizeSelector(array $selector): array
    {
        return array_map(
            fn($alias, $column): string => $column . ' as ' . $alias,
            array_keys($selector),
            $selector
        );
    }
}
