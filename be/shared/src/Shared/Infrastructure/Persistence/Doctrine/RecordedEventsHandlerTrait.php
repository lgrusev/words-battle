<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Persistence\Doctrine;

use Leonix\Shared\Domain\Bus\Event\ContainDomainEvents;
use Leonix\Shared\Domain\Bus\Event\DomainEvent;
use Leonix\Shared\Domain\Bus\Event\EventBus;

trait RecordedEventsHandlerTrait
{
    /**
     * @var EventBus
     */
    private $eventBus;
    /**
     * @var ContainDomainEvents
     */
    private $events;

    private function handleEvents(DomainEvent $afterFlushEvent = null): void
    {
        $events = $this->events->recordedEvents();
        $this->events->eraseEvents();
        foreach ($events as $event) {
            $this->eventBus->notify($event);
        }

        if ($afterFlushEvent === null) {
            return;
        }

        $this->eventBus->notify($afterFlushEvent);
    }
}
