<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Persistence\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Leonix\Shared\Domain\Bus\Event\ContainDomainEvents;
use Leonix\Shared\Domain\Bus\Event\DomainEvent;
use Leonix\Shared\Domain\Bus\Event\EventBus;
use Leonix\Shared\Infrastructure\Persistence\TransactionalSession;

final class DoctrineTransactionalSession implements TransactionalSession
{
    use RecordedEventsHandlerTrait;

    public function __construct(private readonly EntityManagerInterface $entityManager, EventBus $eventBus, ContainDomainEvents $events)
    {
        $this->eventBus = $eventBus;
        $this->events = $events;
    }

    public function commit(DomainEvent $afterCommitEvent = null): void
    {
        $this->entityManager->flush();
        $this->handleEvents($afterCommitEvent);
    }

    public function commitTransactional(callable $operation, DomainEvent $afterCommitEvent = null)
    {
        return $this->entityManager->transactional(
            function ($em) use ($operation, $afterCommitEvent) {
                $result = $operation($em);
                $this->handleEvents($afterCommitEvent);

                return $result;
            }
        );
    }
}
