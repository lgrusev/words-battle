<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Bus\Event;

use Exception;
use Leonix\Shared\Domain\Bus\Event\DomainEvent;
use Leonix\Shared\Domain\Bus\Event\EventBus;
use Leonix\Shared\Domain\Bus\Event\EventSubscriberNotRegisteredError;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\NoHandlerForMessageException;
use Symfony\Component\Messenger\MessageBusInterface;

class SymfonyEventBusAdapter implements EventBus
{
    public function __construct(private readonly MessageBusInterface $bus)
    {
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function notify(DomainEvent $event, array $context = []): void
    {
        $message = $event;
        if (isset($context['stamps'])) {
            if (!\is_array($context['stamps'])) {
                $context['stamps'] = [$context['stamps']];
            }

            $message = new Envelope($message, $context['stamps']);
        }

        try {
            $this->bus->dispatch($message);
        } catch (NoHandlerForMessageException) {
            throw new EventSubscriberNotRegisteredError($event);
        } catch (HandlerFailedException $error) {
            throw $error->getPrevious() ?? $error;
        }
    }
}
