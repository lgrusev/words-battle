<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Bus\Query;

use Exception;
use Leonix\Shared\Domain\Bus\Query\Query;
use Leonix\Shared\Domain\Bus\Query\QueryBus;
use Leonix\Shared\Domain\Bus\Query\QueryNotRegisteredError;
use Leonix\Shared\Domain\Bus\Query\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\NoHandlerForMessageException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class SymfonyQueryBusAdapter implements QueryBus
{
    public function __construct(private readonly MessageBusInterface $bus)
    {
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function ask(Query $query, array $context = []): ?Response
    {
        $message = $query;
        if (isset($context['stamps'])) {
            if (!\is_array($context['stamps'])) {
                $context['stamps'] = [$context['stamps']];
            }

            $message = new Envelope($message, $context['stamps']);
        }

        try {
            /** @var HandledStamp $stamp */
            $stamp = $this->bus->dispatch($message)->last(HandledStamp::class);

            return $stamp->getResult();
        } catch (NoHandlerForMessageException) {
            throw new QueryNotRegisteredError($query);
        } catch (HandlerFailedException $error) {
            throw $error->getPrevious() ?? $error;
        }
    }
}
