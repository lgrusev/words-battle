<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Api\Response;

use JsonSerializable;
use Leonix\Shared\Application\Response\Api\FailedApiResponse;
use Leonix\Shared\Application\Response\Api\ResponseInterface;
use Leonix\Shared\Application\Response\Api\SuccessApiResponse;
use Leonix\Shared\Infrastructure\Api\ApiVersionNegotiator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Traversable;

final class JsonApiResponseBuilder
{
    public function __construct(private readonly ApiVersionNegotiator $versionNegotiator)
    {
    }

    public function build(Request $request, ResponseInterface $responseData): JsonResponse
    {
        $version = $this->versionNegotiator->getVersion($request);
        if ($version === 'v1') {
            return $this->buildLegacyResponse($request, $responseData);
        }

        $body = $this->getNormalizedResponseBody($responseData);

        return new JsonResponse($body, $responseData->status());
    }

    /**
     * @noinspection PhpDeprecationInspection
     */
    /**
     * @psalm-suppress PossiblyInvalidArgument
     */
    public function getNormalizedResponseBody(ResponseInterface $responseData): array
    {
        $body = $responseData->body();
        // Normalize data artifacts for backward compatibility
        if (isset($body['data']['data'])) {
            $data = $body['data'];
            $body = array_merge($body, $data);
        }

        if (isset($body['error']['data'][0]) && $body['error']['data'][0] === $body['error']['message']) {
            $body['error']['data'] = [];
        }

        return $body;
    }

    private function buildLegacyResponse(Request $request, ResponseInterface $controllerResult): JsonResponse
    {
        $cmd = $request->get('cmd') ?? 'unknown';
        $bcHint = $controllerResult->bcHint();
        if ($controllerResult instanceof FailedApiResponse) {
            if ($bcHint === FailedApiResponse::BC_HINT_RESULT_FAILURE) {
                $data = $controllerResult->error()->data();
                $data = empty($data) ? [$controllerResult->error()->message()] : $data;

                return JsonApi::resultFailure($data);
            }

            $data = $controllerResult->error()->data();
            if ($data !== []) {
                /**
                 * Legacy code example
                 * return JsonApi::apiResultFailure($cmd, ['player' => $e->getMessage()]);
                 */
                return JsonApi::apiResultFailure(
                    $cmd,
                    $data
                );
            }

            return JsonApi::apiResultFailure(
                $cmd,
                [
                    'code' => $controllerResult->error()->code(),
                    'message' => $controllerResult->error()->message(),
                ]
            );
        }

        $data = $controllerResult->body()['data'];
        if ($bcHint === SuccessApiResponse::BC_HINT_RESULT_OK) {
            return JsonApi::resultOk($data);
        }

        if ($bcHint === SuccessApiResponse::BC_HINT_OK_DATA) {
            return JsonApi::okData($data);
        }

        if (!is_array($data) && $data instanceof Traversable) {
            $data = iterator_to_array($data);
        } elseif ($data instanceof JsonSerializable) {
            $data = $data->jsonSerialize();
        }

        return JsonApi::apiResultOk($cmd, $data);
    }
}
