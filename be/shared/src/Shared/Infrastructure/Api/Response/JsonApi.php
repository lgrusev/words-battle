<?php

namespace Leonix\Shared\Infrastructure\Api\Response;

use Leonix\Shared\Infrastructure\Symfony\Helper\ConstraintViolationListNormalizer;
use Leonix\Shared\Infrastructure\Symfony\Helper\FormErrorIteratorNormalizer;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @deprecated
 */
class JsonApi
{
    final public const VALIDATION_FIELD_INVALID = 'invalid';

    public static function okData(array $data): JsonResponse
    {
        return self::resultOk(['data' => $data]);
    }

    public static function resultOk(array $data = []): JsonResponse
    {
        return new JsonResponse(array_merge(['status' => 'ok'], $data));
    }

    public static function validationError(ConstraintViolationListInterface $violationList): JsonResponse
    {
        return self::resultFailure(ConstraintViolationListNormalizer::toArray($violationList));
    }

    public static function resultFailure($errors = []): JsonResponse
    {
        $template = ['status' => 'failure'];

        if (\is_array($errors)) {
            $template['errors'] = $errors;
        } elseif ($errors instanceof FormErrorIterator && $errors->count()) {
            $template['errors'] = FormErrorIteratorNormalizer::toArray($errors);
        }

        return new JsonResponse($template);
    }

    public static function invalidSignature(): JsonResponse
    {
        return self::resultFailure(['signature' => self::VALIDATION_FIELD_INVALID]);
    }

    public static function apiResultOk(string $cmd, array $data = []): JsonResponse
    {
        $template = [
            'cmd' => $cmd,
            'result' => 'ok',
            // result=status to maintain backwards compatibility with mgBackend
            'status' => 'ok',
        ];

        return new JsonResponse(array_merge($template, $data));
    }

    /**
     * @param $cmd
     */
    public static function apiResultFailure($cmd, iterable $errors = []): JsonResponse
    {
        $template = [
            'cmd' => $cmd,
            'result' => 'error',
            // result=status to maintain backwards compatibility with mgBackend
            'status' => 'error',
        ];

        if (\is_array($errors)) {
            $template['validationErrors'] = $errors;
        } elseif ($errors instanceof FormErrorIterator && $errors->count()) {
            $template['validationErrors'] = FormErrorIteratorNormalizer::toArray($errors);
        }

        return new JsonResponse($template);
    }
}
