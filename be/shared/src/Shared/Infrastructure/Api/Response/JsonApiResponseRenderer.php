<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Api\Response;

use Leonix\Shared\Application\Response\Api\ResponseInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class JsonApiResponseRenderer implements EventSubscriberInterface
{
    public function __construct(private readonly JsonApiResponseBuilder $responseBuilder)
    {
    }

    public function __invoke(ViewEvent $viewEvent): void
    {
        $controllerResult = $viewEvent->getControllerResult();
        if (!$controllerResult instanceof ResponseInterface) {
            return;
        }

        $request = $viewEvent->getRequest();
        $viewEvent->setResponse($this->responseBuilder->build($request, $controllerResult));
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['__invoke'],
        ];
    }
}
