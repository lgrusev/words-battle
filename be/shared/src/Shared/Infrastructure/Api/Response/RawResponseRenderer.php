<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Api\Response;

use Leonix\Shared\Application\Enum\ResponseType;
use Leonix\Shared\Application\Response\Web\RawResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class RawResponseRenderer implements EventSubscriberInterface
{
    public function __invoke(ViewEvent $viewEvent): void
    {
        $controllerResult = $viewEvent->getControllerResult();
        if (!$controllerResult instanceof RawResponse) {
            return;
        }

        $viewEvent->setResponse(
            new Response(
                $controllerResult->body(),
                $controllerResult->status(),
                ['content-type' => ResponseType::contentTypeFrom($controllerResult->type())],
            )
        );
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['__invoke'],
        ];
    }
}
