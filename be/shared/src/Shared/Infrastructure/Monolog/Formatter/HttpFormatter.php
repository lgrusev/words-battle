<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Monolog\Formatter;

use Monolog\Formatter\LineFormatter;

final class HttpFormatter extends LineFormatter
{
    private const BEGIN_FORMAT = "---------------------------BEGIN [%datetime%] %channel%.%level_name%%context.exec_time% %extra.X-Request-ID%-------------------------\n";
    private const BODY_FORMAT = '%message%';
    private const END_FORMAT = "\n----------------------------------END---------------------------------\n\n";
    private const SHORT_FORMAT = '[%datetime%] %channel%.%level_name%%context.exec_time% %extra.X-Request-ID% %message%';

    public function __construct(private readonly bool $expanded = false)
    {
        $format = $expanded ? self::BEGIN_FORMAT . self::BODY_FORMAT . self::END_FORMAT : self::SHORT_FORMAT;
        parent::__construct($format, null, true, true);
    }

    public function format(array $record): string
    {
        $record['context']['exec_time'] = ' exec_time: ' . ($record['context']['exec_time'] ?? '-') . ' ';
        $formatted = parent::format($record);
        if ($this->expanded) {
            return $formatted . "\n";
        }

        return str_replace(["\r", "\n"], ['\r', '\n'], $formatted) . "\n";
    }
}
