<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class MiddlewareHandler implements RequestHandlerContract
{
    public function __construct(private readonly RequestMiddlewareContract $middleware, private readonly RequestHandlerContract $handler)
    {
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        return $this->middleware->process($request, $this->handler);
    }
}
