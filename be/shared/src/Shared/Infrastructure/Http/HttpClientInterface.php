<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface
{
    /**
     * @throws HttpClientException
     */
    public function get(string $url): ResponseInterface;

    /**
     * @throws HttpClientException
     */
    public function post(
        string $url,
        ?string $body = null,
        array $headers = []
    ): ResponseInterface;

    /**
     * @throws HttpClientException
     */
    public function request(
        string $method,
        string $url,
        ?string $body = null,
        array $headers = [],
        bool $stream = false
    ): ResponseInterface;
}
