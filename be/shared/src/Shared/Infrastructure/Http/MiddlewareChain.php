<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class MiddlewareChain implements RequestHandlerContract, RequestChainMiddlewareContract
{
    public function __construct(private RequestHandlerContract $defaultHandler = new DefaultHandler(), private array $chain = [])
    {
    }

    public function getDefaultHandler(): RequestHandlerContract
    {
        return $this->defaultHandler;
    }

    public function setDefaultHandler(RequestHandlerContract $requestHandler): void
    {
        $this->defaultHandler = $requestHandler;
    }

    /**
     * Adds middleware to the chain.
     */
    public function add(RequestMiddlewareContract $middleware): void
    {
        $this->chain[] = $middleware;
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        $chain = $this->buildChain();

        return $chain->handle($request);
    }

    /**
     * Builds the request handler chain.
     */
    private function buildChain(): RequestHandlerContract
    {
        $chain = $this->defaultHandler;

        foreach (array_reverse($this->chain) as $middleware) {
            $chain = new MiddlewareHandler($middleware, $chain);
        }

        return $chain;
    }
}
