<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;

class DefaultHandler implements RequestHandlerContract
{
    private readonly ResponseFactoryInterface $responseFactory;

    public function __construct(?ResponseFactoryInterface $responseFactory = null)
    {
        $this->responseFactory = $responseFactory ?? Psr17FactoryDiscovery::findResponseFactory();
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        return $this->responseFactory->createResponse(404, 'Not Found');
    }
}
