<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

interface RequestChainMiddlewareContract
{
    public function add(RequestMiddlewareContract $middleware): void;
}
