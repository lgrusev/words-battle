<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface RequestMiddlewareContract
{
    public function process(RequestInterface $request, RequestHandlerContract $handler): ResponseInterface;

    public function name(): string;
}
