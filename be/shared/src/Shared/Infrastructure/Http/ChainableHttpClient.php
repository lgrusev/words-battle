<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Webmozart\Assert\Assert;

class ChainableHttpClient implements HttpClientInterface, RequestHandlerContract, RequestChainMiddlewareContract
{
    private MiddlewareChain $middlewareChain;

    public function __construct(
        RequestHandlerContract $client,
        private readonly ?RequestFactoryInterface $requestFactory = null,
        private readonly ?StreamFactoryInterface $streamFactory = null
    ) {
        $this->middlewareChain = new MiddlewareChain($client);
    }

    public function withChain(RequestMiddlewareContract $middleware): self
    {
        $client = clone $this;
        $client->add($middleware);

        return $client;
    }

    public function add(RequestMiddlewareContract $middleware): void
    {
        $this->middlewareChain->add($middleware);
    }

    public function withOnlyChain(RequestMiddlewareContract $middleware): self
    {
        // Clone and clear all middlewares
        $client = clone $this;
        $client->middlewareChain = new MiddlewareChain($this->middlewareChain->getDefaultHandler());
        $client->add($middleware);

        return $client;
    }

    public function get(string $url): ResponseInterface
    {
        return $this->request('GET', $url);
    }

    public function request(
        string $method,
        string $url,
        ?string $body = null,
        array $headers = [],
        bool $stream = false
    ): ResponseInterface {
        $requestFactory = $this->requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        Assert::isInstanceOf($requestFactory, RequestFactoryInterface::class);
        $request = $requestFactory->createRequest($method, $url);

        if ($body !== null) {
            $streamFactory = $this->streamFactory ?? Psr17FactoryDiscovery::findStreamFactory();
            Assert::isInstanceOf($streamFactory, StreamFactoryInterface::class);
            $request = $request->withBody($streamFactory->createStream($body));
        }

        foreach ($headers as $name => $value) {
            $request = $request->withHeader($name, $value);
        }

        return $this->handle($request);
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        return $this->middlewareChain->handle($request);
    }

    public function post(string $url, ?string $body = null, array $headers = []): ResponseInterface
    {
        return $this->request('POST', $url, $body, $headers);
    }
}
