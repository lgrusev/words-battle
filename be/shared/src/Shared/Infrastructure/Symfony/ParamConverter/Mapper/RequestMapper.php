<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\ParamConverter\Mapper;

use Leonix\Shared\Infrastructure\Symfony\ParamConverter\Annotation\RequestParamMapper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Exception\MissingConstructorArgumentsException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Throwable;

class RequestMapper implements RequestMapperInterface
{
    private readonly array $defaultContext;

    public function __construct(
        private readonly DenormalizerInterface $serializer,
        array $defaultContext = []
    ) {
        $this->defaultContext = array_merge($defaultContext, ['skip_null_values' => true]);
    }

    /**
     * {@inheritDoc}
     */
    public function map(Request $request, RequestParamMapper $configuration): mixed
    {
        $data = $this->extractData($request, (bool)$configuration->convertEmptyStringToNull) ?? [];
        $context = array_merge($this->defaultContext, $configuration->deserializationContext);
        if ($configuration->toExistedObject) {
            $object = $request->attributes->get($configuration->param);
            $context = array_merge(['object_to_populate' => $object], $context);
        }

        try {
            $object = $this->serializer->denormalize($data, $configuration->class, null, $context);
        } catch (MissingConstructorArgumentsException $exception) {
            throw $exception;
        } catch (Throwable $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        }

        return $object;
    }

    /**
     * Return an array of data from request.
     */
    protected function extractData(Request $request, bool $convertEmptyStringToNull): ?array
    {
        $default = $request->attributes->get('_route_params', []);

        $method = $request->getMethod();
        if ($method === 'GET') {
            $data = array_merge($default, $request->query->all());
            if (!$convertEmptyStringToNull) {
                return $data;
            }

            return $this->normalizeData($data);
        }

        $format = $request->getContentType();
        if ($format === 'json') {
            $data = array_merge($default, json_decode((string)$request->getContent(), true, JSON_THROW_ON_ERROR, JSON_THROW_ON_ERROR) ?? []);
            if (!$convertEmptyStringToNull) {
                return $data;
            }

            return $this->normalizeData($data);
        }

        $params = $request->request->all();
        $files = $request->files->all();

        if (is_array($params) && is_array($files)) {
            $data = array_replace_recursive($params, $files);
        } else {
            $data = $params ?: $files;
        }

        $data = array_merge($default, $data ?? []);
        if (!$convertEmptyStringToNull) {
            return $data;
        }

        return $this->normalizeData($data);
    }

    /**
     * @param $data
     */
    private function normalizeData(array $data): array
    {
        foreach ($data as $key => $value) {
            if ($value === '') {
                $data[$key] = null;
            }
        }

        return $data;
    }
}
