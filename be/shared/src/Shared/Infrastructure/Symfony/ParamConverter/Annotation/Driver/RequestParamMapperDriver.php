<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\ParamConverter\Annotation\Driver;

use Doctrine\Common\Annotations\Reader;
use Leonix\Shared\Infrastructure\Symfony\ParamConverter\Annotation\RequestParamMapper;
use Leonix\Shared\Infrastructure\Symfony\ParamConverter\Mapper\RequestMapperInterface;
use Leonix\Shared\Infrastructure\Symfony\ParamConverter\ParamConverterValidationException;
use LogicException;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Exception\MissingConstructorArgumentsException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestParamMapperDriver implements EventSubscriberInterface
{
    private const VALIDATION_ERRORS_ARGUMENT = 'validationErrors';
    private readonly string $defaultValidationErrorsArgument;
    private ?RequestParamMapper $configuration;

    public function __construct(
        private readonly RequestMapperInterface $requestMapper,
        private readonly Reader $reader,
        private readonly ValidatorInterface $validator,
        ?string $validationErrorsArgument = null,
        private readonly array $defaultValidationContext = []
    ) {
        $this->configuration = null;
        $this->defaultValidationErrorsArgument = $validationErrorsArgument ?? self::VALIDATION_ERRORS_ARGUMENT;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => [
                ['prepareConfig', 9999],
                ['mapRequestData', -9999],
            ],
        ];
    }

    /**
     * @throws ReflectionException
     */
    public function prepareConfig(ControllerEvent $event): void
    {
        $controller = $event->getController();
        if (!is_array($controller)) {
            return;
        }

        $object = new ReflectionClass($controller[0]);
        $method = $object->getMethod($controller[1]);

        $configuration = $this->getConfiguration($this->reader->getMethodAnnotations($method));
        if (!$configuration) {
            return;
        }

        $this->configuration = $configuration;
        if (!isset($this->configuration->param, $this->configuration->class)) {
            throw new LogicException(
                sprintf(
                    "No configured annotation options 'param' or 'class' for '%s' action in '%s'",
                    $method->getName(),
                    $object->getName()
                )
            );
        }

        $request = $event->getRequest();
        if (!$this->configuration->toExistedObject && $request->attributes->has($this->configuration->param)) {
            $object = new ReflectionClass($this->configuration->class);
            $request->attributes->set($this->configuration->param, $object->newInstanceWithoutConstructor());
        }
    }

    public function mapRequestData(ControllerEvent $event): void
    {
        if (!(bool)$this->configuration) {
            return;
        }

        $request = $event->getRequest();

        try {
            $object = $this->requestMapper->map($request, $this->configuration);
        } catch (MissingConstructorArgumentsException $exception) {
            if (!$this->configuration->validate) {
                throw $exception;
            }

            $params = explode('"', $exception->getMessage());
            if (!isset($params[1])) {
                throw $exception;
            }

            $violation = new ConstraintViolation(
                'This value should not be blank.',
                '',
                [],
                '',
                $params[3] ?? $params[1] ?? 'param',
                ''
            );
            $errors = new ConstraintViolationList([$violation]);
            $this->throwOrSetErrors($request, $errors);

            return;
        }

        if (!$object) {
            throw new RuntimeException(
                sprintf(
                    'Could not map request data to %s, class: %s',
                    $this->configuration->param,
                    $this->configuration->class
                )
            );
        }

        if ($this->configuration->validate) {
            $context = array_merge($this->defaultValidationContext, $this->configuration->validationContext);
            $errors = $this->validator->validate($object, null, $context);
            $this->throwOrSetErrors($request, $errors);
        }

        $request->attributes->set($this->configuration->param, $object);
    }

    /**
     * @throw ParamConverterValidationException
     */
    public function throwOrSetErrors(
        Request $request,
        ConstraintViolationListInterface $errors
    ): void {
        $validationErrorsArgument = $this->getValidationErrorsArgument();
        $request->attributes->set($validationErrorsArgument, $errors);
        if ($errors->count() === 0) {
            return;
        }

        assert($this->configuration !== null);
        if ($this->configuration->throwValidation) {
            throw ParamConverterValidationException::fromViolationList($errors);
        }
    }

    public function getValidationErrorsArgument(): string
    {
        return $this->configuration->validationErrorsArgument ?? $this->defaultValidationErrorsArgument;
    }

    private function getConfiguration(array $annotations): RequestParamMapper|false
    {
        foreach ($annotations as $configuration) {
            if ($configuration instanceof RequestParamMapper) {
                return $configuration;
            }
        }

        return false;
    }
}
