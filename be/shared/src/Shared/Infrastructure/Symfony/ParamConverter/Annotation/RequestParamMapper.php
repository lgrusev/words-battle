<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\ParamConverter\Annotation;

/**
 * @Annotation
 */
final class RequestParamMapper
{
    /**
     * @var string
     */
    public $param;

    /**
     * @var string
     */
    public $class;

    /**
     * @var bool
     */
    public $toExistedObject = false;

    /**
     * @var array
     */
    public $deserializationContext = [];

    /**
     * @var bool
     */
    public $validate = true;

    /**
     * @var bool
     */
    public $throwValidation = false;

    /**
     * @var array
     */
    public $validationContext = [];

    /**
     * @var string
     */
    public $validationErrorsArgument;

    /**
     * @var bool
     */
    public $convertEmptyStringToNull = true;
}
