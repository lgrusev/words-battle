<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\ParamConverter;

use InvalidArgumentException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

final class ParamConverterValidationException extends InvalidArgumentException
{
    private ?ConstraintViolationListInterface $violationList = null;

    private function __construct($message = '')
    {
        parent::__construct($message);
    }

    public static function fromViolationList(
        ConstraintViolationListInterface $violationList
    ): ParamConverterValidationException {
        $self = new self('Validation error');
        $self->violationList = $violationList;

        return $self;
    }

    public function violationList(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }
}
