<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Logger;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ResponseLoggerService
{
    private const UNLIMMITED = -1;
    private const BLACKLIST = [
        'payment_*',
        'frontend_api_profile_register',
        'frontend_api_password_change',
        'frontend_api_access_authorize',
        'admin_api_profile_edit_profile',
        'frontend_api_profile_edit_profile',
    ];

    public function __construct(private readonly LoggerInterface $logger, private readonly bool $enabled = false, private readonly string $routeFilter = 'default', private readonly ?int $bodyMaxSize = self::UNLIMMITED)
    {
    }

    public function log(Request $request, Response $response): void
    {
        if (!$this->isEnabled()) {
            return;
        }

        $routes = $this->getRoutes($request);
        if ($routes === []) {
            return;
        }

        $pass = false;
        $passedRoute = null;
        foreach ($routes as $route) {
            if (fnmatch($this->routeFilter, $route)) {
                $pass = true;
                $passedRoute = $route;
                break;
            }
        }

        if (!$pass) {
            return;
        }

        $excluded = false;
        $excludedRoute = null;
        assert($passedRoute !== null);
        foreach (self::BLACKLIST as $pattern) {
            if (fnmatch($pattern, $passedRoute)) {
                $excluded = true;
                $excludedRoute = $pattern;
                break;
            }
        }

        $execTime = (int)((microtime(true) - $request->server->get('REQUEST_TIME_FLOAT', 0)) * 1000);
        if ($excluded) {
            $this->logger->debug(
                "Excluded: route '{route}' is matched pattern '{exclude_pattern}'",
                [
                    'route' => $passedRoute,
                    'exclude_pattern' => $excludedRoute,
                    'exec_time' => $execTime . 'ms',
                ]
            );

            return;
        }

        $this->logger->info(
            "Route: {route}\n[REQUEST]:\n{request}\n\n[RESPONSE]:\n{response}\n\n",
            [
                'request' => $this->normalize((string)$request),
                'response' => $this->normalize((string)$response),
                'route' => $passedRoute,
                'exec_time' => $execTime . 'ms',
            ]
        );
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @psalm-suppress DuplicateArrayKey
     */
    private function getRoutes(Request $request): array
    {
        return array_reverse(
            array_filter(
                [...[$request->get('_route')], ...array_values($request->get('_sub_routes', []))]
            )
        );
    }

    private function normalize(string $httpContent): string
    {
        if (strlen($httpContent) <= $this->bodyMaxSize) {
            return $httpContent;
        }

        return substr($httpContent, 0, $this->bodyMaxSize) . "\n--- TRUNCATED ---\n";
    }
}
