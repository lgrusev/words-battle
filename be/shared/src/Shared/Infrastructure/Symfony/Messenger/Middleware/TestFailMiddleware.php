<?php

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Middleware;

use Exception;
use Leonix\Shared\Infrastructure\Symfony\Messenger\Stamp\FailStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;
use Symfony\Component\Messenger\Stamp\SentStamp;

/**
 * !!!!!!!! FOR TEST PURPOSE ONLY
 */
class TestFailMiddleware implements MiddlewareInterface
{
    private const ENV_FAIL_NEXT_MESSAGE = 'TEST_FAIL_NEXT_MESSAGE_N_TIMES';

    /**
     * @throws Exception
     */
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        if ($envelope->last(SentStamp::class) === null) {
            if (($_ENV[self::ENV_FAIL_NEXT_MESSAGE] ?? 0) > 0) {
                if ($envelope->last(FailStamp::class) === null) {
                    $envelope = $envelope->with(new FailStamp($_ENV[self::ENV_FAIL_NEXT_MESSAGE]));
                    $_ENV[self::ENV_FAIL_NEXT_MESSAGE] = null;
                }
            }

            return $stack->next()->handle($envelope, $stack);
        }

        /** @var ReceivedStamp $receivedStamp */
        $receivedStamp = $envelope->last(ReceivedStamp::class);
        if ($receivedStamp && $receivedStamp->getTransportName() === 'sync') {
            return $stack->next()->handle($envelope, $stack);
        }

        /** @var FailStamp $stamp */
        $stamp = $envelope->last(FailStamp::class);
        if ($stamp && $stamp->isShouldFail()) {
            $stamp->fail();
            throw new Exception($stamp->failMessage());
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
