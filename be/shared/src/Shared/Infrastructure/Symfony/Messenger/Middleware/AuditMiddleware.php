<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Middleware;

use Leonix\Shared\Infrastructure\Symfony\Messenger\Stamp\UniqueIdStamp;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;
use Symfony\Component\Messenger\Stamp\SentStamp;

class AuditMiddleware implements MiddlewareInterface
{
    public function __construct(private readonly LoggerInterface $logger, private readonly bool $enabled = false)
    {
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        if (!$this->enabled) {
            return $stack->next()->handle($envelope, $stack);
        }

        if ($envelope->last(UniqueIdStamp::class) === null) {
            $envelope = $envelope->with(new UniqueIdStamp());
        }

        /** @var UniqueIdStamp $stamp */
        $stamp = $envelope->last(UniqueIdStamp::class);
        $context = [
            'id' => $stamp->uniqueId(),
            'class' => $envelope->getMessage()::class,
            'sent_time' => $stamp->time(),
        ];

        $envelope = $stack->next()->handle($envelope, $stack);
        if ($envelope->last(ReceivedStamp::class) !== null) {
            $context['exec_time'] = (microtime(true) - $stamp->time()) . ' s';
            $this->logger->info('[{id}] Received & handling {class}', $context);
        } elseif ($envelope->last(SentStamp::class) !== null) {
            $this->logger->info('[{id}] Sent {class}', $context);
        } else {
            $this->logger->info('[{id}] Handling sinc {class}', $context);
        }

        return $envelope;
    }
}
