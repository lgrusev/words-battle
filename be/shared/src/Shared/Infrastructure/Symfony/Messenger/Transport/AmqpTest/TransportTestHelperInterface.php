<?php

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Transport\AmqpTest;

use Symfony\Component\Messenger\Transport\TransportInterface;

interface TransportTestHelperInterface
{
    public function hasTransport(string $transportName): bool;

    public function getTransport(string $transportName): TransportInterface;

    public function purge(): void;

    public function purgeTransport(string $transportName, ?string $queueName = null): void;

    public function getMessageCount(string $transportName): int;
}
