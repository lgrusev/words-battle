<?php

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Transport\AmqpTest;

use Exception;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\AmqpExt\AmqpReceiver;
use Symfony\Component\Messenger\Transport\AmqpExt\AmqpSender;
use Symfony\Component\Messenger\Transport\AmqpExt\Connection;
use Symfony\Component\Messenger\Transport\Receiver\MessageCountAwareInterface;
use Symfony\Component\Messenger\Transport\Serialization\PhpSerializer;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\SetupableTransportInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

/**
 * !!!!ONLY FOR TEST PURPOSE
 */
class AmqpTestTransport implements TransportInterface, SetupableTransportInterface, MessageCountAwareInterface
{
    private $serializer;
    private $receiver;
    private $sender;

    public function __construct(private readonly Connection $connection, SerializerInterface $serializer = null)
    {
        $this->serializer = $serializer ?? new PhpSerializer();
    }

    /**
     * {@inheritDoc}
     */
    public function setup(): void
    {
        $this->connection->setup();
    }

    /**
     * {@inheritDoc}
     */
    public function get(): iterable
    {
        return ($this->receiver ?? $this->getReceiver())->get();
    }

    /**
     * {@inheritDoc}
     * @throws Exception
     */
    public function ack(Envelope $envelope): void
    {
        ($this->receiver ?? $this->getReceiver())->ack($envelope);
    }

    /**
     * {@inheritDoc}
     */
    public function reject(Envelope $envelope): void
    {
        ($this->receiver ?? $this->getReceiver())->reject($envelope);
    }

    /**
     * {@inheritDoc}
     */
    public function send(Envelope $envelope): Envelope
    {
        return ($this->sender ?? $this->getSender())->send($envelope);
    }

    /**
     * {@inheritDoc}
     */
    public function getMessageCount(): int
    {
        return ($this->receiver ?? $this->getReceiver())->getMessageCount();
    }

    /**
     * !!!!!!!!!! For tests purpose only
     */
    public function purgeQueues(): void
    {
        $this->connection->purgeQueues();
    }

    private function getReceiver(): AmqpReceiver
    {
        return $this->receiver = new AmqpReceiver($this->connection, $this->serializer);
    }

    private function getSender(): AmqpSender
    {
        return $this->sender = new AmqpSender($this->connection, $this->serializer);
    }
}
