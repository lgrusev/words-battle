<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Stamp;

use Symfony\Component\Messenger\Stamp\StampInterface;

class FailStamp implements StampInterface
{
    private const DEFAULT_MESSAGE = 'Test Fail Message {current}';
    private int $failed = 0;
    private $failMessage;

    public function __construct(private readonly int $failTimes, ?string $failMessage = null)
    {
        $this->failMessage = $failMessage ?? self::DEFAULT_MESSAGE;
    }

    public function fail()
    {
        $this->failed++;
    }

    public function isShouldFail(): bool
    {
        return ($this->failTimes - $this->failed) > 0;
    }

    public function failMessage(): string
    {
        return str_replace('{current}', (string)$this->failed, $this->failMessage);
    }
}
