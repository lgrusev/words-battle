<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Helper;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;

final class FormErrorIteratorNormalizer
{
    public static function toArray(FormErrorIterator $errors): array
    {
        $collectedErrors = [];
        if ($errors->count() === 0) {
            return $collectedErrors;
        }

        $errors->rewind();
        while ($errors->valid()) {
            $error = $errors->current();
            if ($error instanceof FormError) {
                $errorMessage = $error->getMessage();
                $errorMessage = self::cleanErrorMessage($errorMessage);
                $origin = $error->getOrigin();
                if ($origin !== null) {
                    $collectedErrors[$origin->getName()] = $errorMessage;
                } else {
                    $collectedErrors[] = $errorMessage;
                }
            } else {
                $errorMessage = (string)$error;
                $errorMessage = self::cleanErrorMessage($errorMessage);
                $collectedErrors[$error->getForm()->getName()] = $errorMessage;
            }

            $errors->next();
        }

        return $collectedErrors;
    }

    private static function cleanErrorMessage(string $errorMessage): string
    {
        return str_replace(['ERROR: ', "\n"], '', $errorMessage);
    }
}
