<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Helper;

use Symfony\Component\Validator\ConstraintViolationListInterface;

final class ConstraintViolationListNormalizer
{
    public static function toArray(ConstraintViolationListInterface $constraintViolationList): array
    {
        $errorsList = [];
        foreach ($constraintViolationList as $error) {
            $propertyPath = $error->getPropertyPath();
            if (isset($errorsList[$propertyPath])) {
                //@ TODO uncomment line below will require tests fixing
                // Or better use sequential validator
                // @see https://symfony.com/doc/current/reference/constraints/Sequentially.html
                //$errorsList[$propertyPath] .= '|' . $error->getMessage();
                continue;
            }

            $errorsList[$propertyPath] = $error->getMessage();
        }

        return $errorsList;
    }
}
