<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Service;

use Leonix\Shared\Domain\Model\Error;

final class ErrorFactory
{
    public static function generalError(): Error
    {
        return new Error('General server error.', 'GENERAL_ERROR');
    }

    public static function validationError(array $errors): Error
    {
        return new Error('Validation error.', 'VALIDATION_ERROR', $errors);
    }
}
