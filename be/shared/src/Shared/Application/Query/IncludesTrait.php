<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

use Symfony\Component\Validator\Constraints as Assert;

trait IncludesTrait
{
    /**
     * CSV relations to include: include=id,title
     *
     * @var string
     */
    #[Assert\Regex(pattern: '/^(?:[a-z0-9.]+)*(?:[a-z0-9.]+,)*(?:[a-z0-9.]+)*$/i', message: 'Invalid include format, expected: include=id,productId')]
    private $include;

    public function includes(array $default = []): array
    {
        if (!$this->include) {
            return $default;
        }

        $includes = array_unique(array_map('trim', explode(',', rtrim($this->include, ','))));

        return $includes ?? $default;
    }
}
