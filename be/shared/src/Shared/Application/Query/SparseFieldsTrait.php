<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

use Symfony\Component\Validator\Constraints as Assert;

trait SparseFieldsTrait
{
    /**
     * CSV fields to return: fields[default]=id,title
     *
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Regex(
     *         pattern="/^(?:[a-z0-9.]+)*(?:[a-z0-9.]+,)*(?:[a-z0-9.]+)*$/i",
     *         message="Invalid fields format, expected: fields[key]=id,productId"
     *     )
     * })
     * @var array
     */
    #[Assert\Type(type: 'array')]
    private $fields;

    public function fields(array $default = [], ?string $resource = null): array
    {
        $resource ??= 'default';
        if (!isset($this->fields[$resource])) {
            return $default;
        }

        return array_unique(array_map('trim', explode(',', (string)rtrim($this->fields[$resource], ','))));
    }

    public function withFields(array $fields, ?string $resource = null)
    {
        $self = clone $this;
        $self->fields = $this->fields ?? [];
        $resource ??= 'default';
        $self->fields[$resource] = $self->parsedFilters[$resource] ?? [];
        $self->fields[$resource] = implode(',', $fields);

        return $self;
    }
}
