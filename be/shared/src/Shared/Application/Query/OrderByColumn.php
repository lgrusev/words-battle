<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

final class OrderByColumn
{
    public function __construct(private readonly string $name, private readonly bool $descendant = false)
    {
    }

    public function name(): string
    {
        return $this->name;
    }

    public function isDescendant(): bool
    {
        return $this->descendant;
    }
}
