<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

final class Page
{
    private const DEFAULT_LIMIT = 20;
    private readonly int $limit;
    private readonly int $skip;

    public function __construct(?int $limit = null, ?int $skip = null)
    {
        $this->limit = $limit ?? self::DEFAULT_LIMIT;
        $this->skip = $skip ?? 0;
    }

    public function limit(): int
    {
        return $this->limit;
    }

    public function skip(): int
    {
        return $this->skip;
    }
}
