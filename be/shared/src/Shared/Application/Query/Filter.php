<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

use InvalidArgumentException;

final class Filter
{
    public const OP_EQ = 1;
    public const OP_NEQ = 2;
    public const OP_GT = 3;
    public const OP_GTE = 4;
    public const OP_LT = 5;
    public const OP_LTE = 6;
    public const OP_IN = 7;
    public const OP_NOT_IN = 8;
    public const OP_STARTS_WITH = 9;
    public const OP_CONTAINS = 10;
    public const OP_ENDS_WITH = 11;

    public const OPERATORS_MAP = [
        'eq' => self::OP_EQ,
        'neq' => self::OP_NEQ,
        'gt' => self::OP_GT,
        'gte' => self::OP_GTE,
        'lt' => self::OP_LT,
        'lte' => self::OP_LTE,
        'in' => self::OP_IN,
        'nin' => self::OP_NOT_IN,
        'startsWith' => self::OP_STARTS_WITH,
        'contains' => self::OP_CONTAINS,
        'endsWith' => self::OP_ENDS_WITH,
    ];
    private readonly string $column;
    /**
     * @var mixed
     */
    private $params;
    private readonly int $operator;

    private function __construct(string $column, $param, int $operator)
    {
        if (!is_array($param) && in_array($operator, [self::OP_IN, self::OP_NOT_IN], false)) {
            throw new InvalidArgumentException(
                sprintf('Expected filter parameter for %s to be an array, got %s', $column, gettype($param))
            );
        }

        $this->column = $column;
        $this->params = $param;
        $this->operator = $operator;
    }

    public static function createFilter(string $operator, string $column, $param): Filter
    {
        if (!isset(self::OPERATORS_MAP[$operator])) {
            throw new InvalidArgumentException('Invalid operator: ' . strip_tags($operator));
        }

        return new self($column, $param, self::OPERATORS_MAP[$operator]);
    }

    public static function equal(string $column, $param): Filter
    {
        return new self($column, $param, self::OP_EQ);
    }

    public static function notEqual(string $column, $param): Filter
    {
        return new self($column, $param, self::OP_NEQ);
    }

    public static function greaterThan(string $column, $param): Filter
    {
        return new self($column, $param, self::OP_GT);
    }

    public static function greaterThanOrEqual(string $column, $param): Filter
    {
        return new self($column, $param, self::OP_GTE);
    }

    public static function lessThan(string $column, $param): Filter
    {
        return new self($column, $param, self::OP_LT);
    }

    public static function lessThanOrEqual(string $column, $param): Filter
    {
        return new self($column, $param, self::OP_LTE);
    }

    public static function in(string $column, array $params): Filter
    {
        return new self($column, $params, self::OP_IN);
    }

    public static function notIn(string $column, array $params): Filter
    {
        return new self($column, $params, self::OP_NOT_IN);
    }

    public static function startsWith(string $column, string $param): Filter
    {
        return new self($column, $param, self::OP_STARTS_WITH);
    }

    public static function contains(string $column, string $param): Filter
    {
        return new self($column, $param, self::OP_CONTAINS);
    }

    public static function endsWith(string $column, string $param): Filter
    {
        return new self($column, $param, self::OP_ENDS_WITH);
    }

    public static function isSupported(string $operator): bool
    {
        return isset(self::OPERATORS_MAP[$operator]);
    }

    public static function isArrayOperator(string $operator): bool
    {
        return in_array($operator, ['in', 'nin'], false);
    }

    public function column(): string
    {
        return $this->column;
    }

    public function params(): mixed
    {
        return $this->params;
    }

    public function operator(): int
    {
        return $this->operator;
    }
}
