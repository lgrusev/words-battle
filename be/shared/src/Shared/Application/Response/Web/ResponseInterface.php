<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Response\Web;

interface ResponseInterface
{
    public function body(): string;

    public function status(): int;

    public function type(): string;
}
