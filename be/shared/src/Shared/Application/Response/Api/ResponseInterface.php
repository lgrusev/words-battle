<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Response\Api;

interface ResponseInterface
{
    public const BC_HINT_NO_HINT = 0;

    public function body(): array;

    public function status(): int;

    /**
     * Should be used to hint backward compatibility flags
     */
    public function withBcHint(int $hint): ResponseInterface;

    /**
     * Return backward compatibility hint
     */
    public function bcHint(): int;
}
