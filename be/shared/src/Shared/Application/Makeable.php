<?php

declare(strict_types=1);

namespace Leonix\Shared\Application;

trait Makeable
{
    /**
     * @param ...$args
     */
    public static function make(...$args): static
    {
        return new static(...$args);
    }
}
