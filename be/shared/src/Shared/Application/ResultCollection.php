<?php

declare(strict_types=1);

namespace Leonix\Shared\Application;

use ArrayIterator;
use JsonSerializable;

use function count;
use function reset;

final class ResultCollection implements ResultCollectionInterface, JsonSerializable
{
    public function __construct(private array $items, private readonly ?string $itemHydrationClass = null)
    {
    }

    public function getResult(): array
    {
        if (!$this->itemHydrationClass) {
            return $this->items;
        }

        return $this->hydrateResultsAs($this->itemHydrationClass)->getResult();
    }

    public function hydrateSingleResultAs(string $className): mixed
    {
        assert(class_exists($className) && is_callable($className . '::fromArray'));
        $item = $this->getSingleResult();

        return $className::fromArray($item);
    }

    public function getSingleResult()
    {
        return reset($this->items);
    }

    public function hydrateResultsAs(string $className): ResultCollectionInterface
    {
        assert(class_exists($className) && is_callable($className . '::fromArray'));
        $hydratedItems = [];
        foreach ($this->items as $item) {
            $hydratedItems[] = $className::fromArray($item);
        }

        return new self($hydratedItems);
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->getResult());
    }

    public function jsonSerialize(): array
    {
        return $this->items;
    }
}
