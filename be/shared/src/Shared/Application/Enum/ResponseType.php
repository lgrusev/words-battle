<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Enum;

final class ResponseType
{
    public const XML = 'xml';
    public const HTML = 'html';
    public const JSON = 'json';

    private const DEFAULT_TYPE = self::HTML;

    private const MIME_TYPES = [
        self::XML => 'application/xml',
        self::HTML => 'text/html',
        self::JSON => 'application/json',
    ];

    public static function contentTypeFrom(string $format): string
    {
        return self::MIME_TYPES[$format] ?? self::MIME_TYPES[self::DEFAULT_TYPE];
    }
}
