<?php

namespace Leonix\Shared\Application;

use RuntimeException;

class EntityId
{
    public function __construct(private $id = null)
    {
    }

    public function id(): int
    {
        if (!$this->id) {
            throw new RuntimeException('Id does not initialized');
        }

        return $this->id;
    }
}
