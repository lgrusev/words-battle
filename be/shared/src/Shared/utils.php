<?php

declare(strict_types=1);

namespace Leonix\Shared;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Throwable;
use Traversable;
use Webmozart\Assert\Assert;

function nullable_int($value): ?int
{
    return $value === null ? null : (int)$value;
}

function nullable_string($value): ?string
{
    return $value === null ? null : (string)$value;
}

function nullable_dateTime($value, ?string $format = null): ?DateTime
{
    return $value === null ? null : DateTime::createFromFormat($format ?? 'Y-m-d H:i:s', $value);
}

function int_to_decimal(int $int, int $scale = 0): ?string
{
    $divisor = 10 ** $scale;

    return bcdiv((string)$int, (string)$divisor, $scale);
}

/**
 * Should be used for money math
 */
function decimal_to_int(string $decimal, int $scale = 0): int
{
    Assert::numeric($decimal);
    $multiplier = 10 ** $scale;

    return (int)bcmul($decimal, (string)$multiplier);
}

function nullable_dateTimeImmutable($value, ?string $format = null): ?DateTimeImmutable
{
    if ($value === null) {
        return null;
    }

    $date = DateTimeImmutable::createFromFormat($format ?? 'Y-m-d H:i:s', $value);

    return $date ?: null;
}

function format_dateTime(?DateTimeInterface $date, ?string $format = null): ?string
{
    return $date === null ? null : $date->format($format ?? 'Y-m-d H:i:s');
}

function immutable_dateTime_from(?DateTimeInterface $date): DateTimeImmutable
{
    if ($date instanceof DateTimeImmutable) {
        return $date;
    }

    \assert($date instanceof DateTime);

    return DateTimeImmutable::createFromMutable($date);
}

function default_if_empty($value, $default)
{
    $result = empty($value) ? $default : $value;

    return is_callable($result) ? $result() : $result;
}

function strip_cider(string $ip): string
{
    $pos = strpos($ip, '/');
    if ($pos === false) {
        return $ip;
    }

    return substr($ip, 0, $pos);
}

function assign_array_by_path(array &$arr, string $path, $value, string $separator = '.'): void
{
    $keys = explode($separator, $path);

    foreach ($keys as $key) {
        $key = trim($key, '[]');
        $arr = &$arr[$key];
    }

    $arr = $value;
}

function stringify_error(Throwable $error, ?string $prefix = null, bool $withTrace = true): string
{
    if (!$withTrace) {
        return sprintf(
            '%s %s %s:%d',
            ($prefix ?? ''),
            $error->getMessage(),
            $error->getFile(),
            $error->getLine(),
        );
    }

    return sprintf(
        '%s %s %s:%d, trace: %s',
        ($prefix ?? ''),
        $error->getMessage(),
        $error->getFile(),
        $error->getLine(),
        $error->getTraceAsString()
    );
}

function append_url_query(string $url, array $params): string
{
    /**
     * @var array<string, string> $parsed
     */
    $parsed = parse_url($url);
    parse_str($parsed['query'] ?? '', $query);
    $query = http_build_query($query ? array_merge($query, $params) : $params);
    $parsed['query'] = $query;

    return build_url($parsed);
}

function build_url(array $parts): string
{
    return (isset($parts['scheme']) ? "{$parts['scheme']}:" : '') .
        ((isset($parts['user']) || isset($parts['host'])) ? '//' : '') .
        (isset($parts['user']) ? (string)($parts['user']) : '') .
        (isset($parts['pass']) ? ":{$parts['pass']}" : '') .
        (isset($parts['user']) ? '@' : '') .
        (isset($parts['host']) ? (string)($parts['host']) : '') .
        (isset($parts['port']) ? ":{$parts['port']}" : '') .
        (isset($parts['path']) ? (string)($parts['path']) : '') .
        (isset($parts['query']) ? "?{$parts['query']}" : '') .
        (isset($parts['fragment']) ? "#{$parts['fragment']}" : '');
}

function safe_explode(string $delimiter, string $value, int $expectedElements = 2, $defaultValue = null): array
{
    return array_pad(explode($delimiter, $value), $expectedElements, $defaultValue);
}

function digest_to_int(string $value): int
{
    return (int)((float)substr((string)hexdec(md5(strrev($value))), 0, 9) * 100_000_000
        . (float)substr((string)hexdec(md5($value)), 0, 9) * 100_000_000);
}

function iterable_to_array(iterable $iterable): array
{
    if ($iterable instanceof Traversable) {
        return iterator_to_array($iterable);
    }

    return (array)$iterable;
}

function iterable_map(iterable $iterable, callable $callback): array
{
    return array_map(static fn($item) => $callback($item), iterable_to_array($iterable));
}
