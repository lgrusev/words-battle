<?php

declare(strict_types=1);

namespace App\Query\Sql;

use App\DTO\WordView;
use App\Query\WordsQueryInterface;
use Doctrine\DBAL\Query\QueryBuilder;
use Leonix\Shared\Application\ResultCollectionInterface;
use Webmozart\Assert\Assert;

final class WordsQuery extends AbstractSqlQuery implements WordsQueryInterface
{
    public function getPage(
        string $language,
        int $page = 1,
        int $limit = self::PAGE_LIMIT,
        ?string $searchTerm = null
    ): ResultCollectionInterface {
        Assert::greaterThanEq($page, 1);
        Assert::greaterThan($limit, 0);

        $limit = min($limit, self::PAGE_MAX_LIMIT);
        $qb = $this->qb($language, $searchTerm)
            ->addSelect('id, word, description')
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        if ($searchTerm) {
            $qb->addSelect('MATCH(word, description) AGAINST (:searchExp in boolean mode) as relevance')
                ->orderBy('word_relevance', 'DESC')
                ->addOrderBy('relevance', 'DESC');
        }

        return $this->fetch([$qb]);
    }

    public function count(string $language, ?string $searchTerm = null): int
    {
        return (int)$this->qb($language, $searchTerm)->select('count(id)')->fetchOne();
    }

    protected function qb(string $language, ?string $searchTerm = null): QueryBuilder
    {
        $qb = parent::queryBuilder()
            ->from('dictionary')
            ->where('language = :language')
            ->setParameter('language', $language);

        if ($searchTerm) {
            $searchTerm = strtolower($searchTerm);
            $qb->addSelect('MATCH(word, description) AGAINST (:searchExp in boolean mode) as relevance')
                ->addSelect('MATCH(word) AGAINST (:search in boolean mode) as word_relevance')
                ->setParameter('search', $searchTerm)
                ->setParameter('searchExp', "$searchTerm*")
                ->andWhere('MATCH(word, description) AGAINST (:searchExp in boolean mode) > 0');
        }

        return $qb;
    }

    protected function getResultItemClass(): ?string
    {
        return WordView::class;
    }
}
