<?php

declare(strict_types=1);

namespace App\Query\Sql;

use App\DTO\LeaderboardItem;
use App\Query\UserSearchQueryInterface;
use Leonix\Shared\Application\ResultCollection;
use Leonix\Shared\Application\ResultCollectionInterface;
use Webmozart\Assert\Assert;

final class UserSearchQuery extends AbstractSqlQuery implements UserSearchQueryInterface
{
    public function search(string $loginOrEmail, int $limit = self::SEARCH_LIMIT): ResultCollectionInterface
    {
        Assert::notNull($loginOrEmail);
        if ($loginOrEmail === '' || $loginOrEmail === '0') {
            return new ResultCollection([]);
        }

        $qb = $this->queryBuilder()
            ->select('login', '`rank`', 'elo')
            ->from('users')
            ->where('login like :login')
            ->orWhere('email like :email')
            ->setParameters(['login' => "%$loginOrEmail%", 'email' => "%$loginOrEmail%"])
            ->setFirstResult(0)
            ->setMaxResults($limit);

        return $this->fetch([$qb]);
    }

    public function count(): int
    {
        return (int)$this->connection->fetchOne('select count(*) from users');
    }

    protected function getResultItemClass(): ?string
    {
        return LeaderboardItem::class;
    }
}
