<?php

declare(strict_types=1);

namespace App\Query\Sql;

use App\DTO\LeaderboardItem;
use App\Query\LeaderboardQueryInterface;
use Leonix\Shared\Application\ResultCollectionInterface;
use Webmozart\Assert\Assert;

final class LeaderboardQuery extends AbstractSqlQuery implements LeaderboardQueryInterface
{
    public function getPage(int $page = 1, int $limit = self::PAGINATOR_PER_PAGE): ResultCollectionInterface
    {
        Assert::greaterThanEq($page, 1);
        Assert::greaterThan($limit, 0);

        $firstResult = ($page - 1) * $limit;
        $qb = $this->queryBuilder()
            ->select('login', '`rank`', 'elo')
            ->from('users')
            ->orderBy('`rank`', 'ASC')
            ->addOrderBy('registered_at')
            ->setFirstResult($firstResult)
            ->setMaxResults($limit);

        return $this->fetch([$qb]);
    }

    public function count(): int
    {
        return (int)$this->connection->fetchOne('select count(*) from users');
    }

    protected function getResultItemClass(): ?string
    {
        return LeaderboardItem::class;
    }
}
