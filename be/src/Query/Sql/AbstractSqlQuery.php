<?php

declare(strict_types=1);

namespace App\Query\Sql;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Result;
use Leonix\Shared\Application\ResultCollection;
use Leonix\Shared\Application\ResultCollectionInterface;
use Webmozart\Assert\Assert;

abstract class AbstractSqlQuery
{
    public function __construct(protected Connection $connection)
    {
    }

    /**
     * @throws Exception
     */
    protected function fetch(array $queries): ResultCollectionInterface
    {
        Assert::minCount($queries, 1);
        $dataQuery = array_pop($queries);
        foreach ($queries as $query) {
            $this->execute($query);
        }

        return new ResultCollection(
            $this->fetchAllAssociative($dataQuery),
            $this->getResultItemClass()
        );
    }

    protected function fetchAllAssociative(string|QueryBuilder $query): array
    {
        if (is_string($query)) {
            return $this->execute($query)->fetchAllAssociative();
        }

        return $query->executeQuery()->fetchAllAssociative();
    }

    protected function execute(string|QueryBuilder $query): Result
    {
        if (is_string($query)) {
            return $this->connection->executeQuery($query);
        }

        return $query->executeQuery();
    }

    protected function getResultItemClass(): ?string
    {
        return null;
    }

    protected function queryBuilder(): QueryBuilder
    {
        return $this->connection->createQueryBuilder();
    }
}
