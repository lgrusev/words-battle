<?php

declare(strict_types=1);

namespace App\Query;

use Leonix\Shared\Application\ResultCollectionInterface;

interface WordsQueryInterface
{
    public const PAGE_LIMIT = 20;
    public const PAGE_MAX_LIMIT = 100;

    public function getPage(
        string $language,
        int $page = 1,
        int $limit = self::PAGE_LIMIT,
        ?string $searchTerm = null
    ): ResultCollectionInterface;

    public function count(string $language, ?string $searchTerm = null): int;
}
