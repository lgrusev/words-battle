<?php

namespace App\Twig;

use DateTimeInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            new TwigFilter('timeago', fn($datetime): string => $this->timeAgoFilter($datetime), ['is_safe' => ['html']]),
            new TwigFilter('highlight', fn(string $text, array $terms, string $class = 'BG-WARNING'): string => $this->highlightFilter($text, $terms, $class), ['is_safe' => ['html']]),
        ];
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('function_name', [$this, 'doSomething']),
        ];
    }

    public function highlightFilter(string $text, array $terms, string $class = 'bg-warning'): string
    {
        $highlight = [];
        foreach ($terms as $term) {
            $highlight[] = '<span class="' . $class . '">' . $term . '</span>';
        }

        return str_ireplace($terms, $highlight, $text);
    }

    public function timeAgoFilter($datetime): string
    {
        if (is_numeric($datetime)) {
            $time = time() - (int)$datetime;
        } elseif ($datetime instanceof DateTimeInterface) {
            $time = time() - $datetime->getTimestamp();
        } else {
            $time = time() - strtotime($datetime);
        }

        $units = [
            31_536_000 => 'year',
            2_592_000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second',
        ];

        foreach ($units as $unit => $val) {
            if ($time < $unit) {
                continue;
            }

            $numberOfUnits = floor($time / $unit);

            return ($val === 'second') ? 'a few seconds ago' : (($numberOfUnits > 1) ? $numberOfUnits : 'a') . ' ' . $val . (($numberOfUnits > 1) ? 's' : '') . ' ago';
        }

        return '-';
    }
}
