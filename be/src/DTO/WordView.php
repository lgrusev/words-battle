<?php

declare(strict_types=1);

namespace App\DTO;

use Leonix\Shared\Application\ConstructableFromArrayTrait;
use Leonix\Shared\Application\ConstructFromArrayInterface;

final class WordView implements ConstructFromArrayInterface
{
    use ConstructableFromArrayTrait;

    public function __construct(private readonly string $word, private readonly string $description)
    {
    }

    public function word(): string
    {
        return $this->word;
    }

    public function description(): string
    {
        return $this->description;
    }
}
