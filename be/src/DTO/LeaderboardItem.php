<?php

declare(strict_types=1);

namespace App\DTO;

use Leonix\Shared\Application\ConstructableFromArrayTrait;

final class LeaderboardItem
{
    use ConstructableFromArrayTrait;

    public function __construct(public string $login, public int $rank, public int $elo)
    {
    }
}
