<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\DictionaryRepository;
use Doctrine\ORM\Mapping as ORM;
use Stringable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DictionaryRepository::class)]
#[UniqueEntity(fields: ['word', 'language'])]
#[ORM\Table(name: 'dictionary', indexes: [new ORM\Index(name: 'language_idx', columns: ['language']), new ORM\Index(name: 'word_full_idx', columns: ['word'], flags: ['fulltext']), new ORM\Index(name: 'word_description_full_idx', columns: ['word', 'description'], flags: ['fulltext'])], uniqueConstraints: [new ORM\UniqueConstraint(columns: ['word', 'language'])])]
class Dictionary implements Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    #[ORM\Column(type: 'string', length: 50)]
    #[Assert\NotBlank]
    #[Assert\Regex(pattern: '/^[a-za-я-]+$/', message: 'Only lowercase letters are allowed')]
    private $word;
    #[ORM\Column(type: 'string', length: 2)]
    #[Assert\NotBlank]
    #[Assert\Language]
    private $language;
    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    private $description;
    #[ORM\Column(type: 'boolean')]
    #[Assert\NotBlank]
    private bool $active = true;

    public function __toString(): string
    {
        return (string)$this->word;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
