<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\GameStatsRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Stringable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: GameStatsRepository::class)]
#[UniqueEntity(fields: ['user_id', 'game_id', 'round_id'])]
#[ORM\Table(name: 'game_stats', indexes: [new ORM\Index(name: 'game_idx', columns: ['game_id', 'user_id', 'round_id'])], uniqueConstraints: [new ORM\UniqueConstraint(columns: ['user_id', 'game_id', 'round_id'])])]
class GameStats implements Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'rounds')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    private $game;
    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\GreaterThan(0)]
    private $roundId;
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    private $user;
    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\GreaterThan(-1)]
    private $score;
    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\Range(min: -9_999_999, max: 9_999_999)]
    private $elo;
    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\Range(min: -9_999_999, max: 9_999_999)]
    private $eloBefore;
    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\Range(min: -9_999_999, max: 9_999_999)]
    private $eloAfter;
    #[ORM\Column(type: 'datetime_immutable')]
    private $startedAt;
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $finishedAt;

    public function __construct()
    {
        $this->startedAt = new DateTimeImmutable();
    }

    public function __toString(): string
    {
        return sprintf(
            '#%d:%d %s  %s ELO: %d, Score: %d',
            $this->game->getId(),
            $this->roundId,
            $this->startedAt->format('Y-m-d H:i:s'),
            $this->user->getLogin(),
            $this->elo,
            $this->score
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getRoundId(): ?int
    {
        return $this->roundId;
    }

    public function setRoundId(int $roundId): self
    {
        $this->roundId = $roundId;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getElo(): ?int
    {
        return $this->elo;
    }

    public function setElo(int $elo): self
    {
        $this->elo = $elo;

        return $this;
    }

    public function getEloBefore(): ?int
    {
        return $this->eloBefore;
    }

    public function setEloBefore(int $eloBefore): self
    {
        $this->eloBefore = $eloBefore;

        return $this;
    }

    public function getEloAfter(): ?int
    {
        return $this->eloAfter;
    }

    public function setEloAfter(int $eloAfter): self
    {
        $this->eloAfter = $eloAfter;

        return $this;
    }

    public function getStartedAt(): ?DateTimeImmutable
    {
        return $this->startedAt;
    }

    public function setStartedAt(DateTimeImmutable $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getFinishedAt(): ?DateTimeImmutable
    {
        return $this->finishedAt;
    }

    public function setFinishedAt(?DateTimeImmutable $finishedAt): self
    {
        $this->finishedAt = $finishedAt;

        return $this;
    }
}
