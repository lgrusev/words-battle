<?php

namespace App\Controller\Admin;

use App\Entity\Dictionary;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

final class DictionaryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Dictionary::class;
    }

    /**
     * @inheritDoc
     */
    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->onlyOnIndex();
        yield TextField::new('word');
        yield TextField::new('language');
        yield BooleanField::new('active')->renderAsSwitch(false);
    }
}
