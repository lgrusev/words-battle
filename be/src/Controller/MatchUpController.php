<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class MatchUpController extends AbstractController
{
    #[Route(path: '/{_locale<%app.supported_locales%>}/match/up', name: 'match_up')]
    public function index(): Response
    {
        return $this->render('match_up/index.html.twig', [
            'controller_name' => 'MatchUpController',
        ]);
    }
}
