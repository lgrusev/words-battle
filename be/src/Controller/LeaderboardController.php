<?php

declare(strict_types=1);

namespace App\Controller;

use App\Query\LeaderboardQueryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class LeaderboardController extends AbstractController
{
    #[Route(path: '/{_locale<%app.supported_locales%>}/', name: 'homepage')]
    #[Route(path: '/{_locale<%app.supported_locales%>}/leaderboard', name: 'leaderboard')]
    public function index(Request $request, LeaderboardQueryInterface $query): Response
    {
        $isAjax = $request->isXmlHttpRequest() || (int)$request->query->get('ajax') === 1;
        $page = max(1, $request->query->getInt('page', 1));
        $limit = max(1, $request->query->getInt('limit', LeaderboardQueryInterface::PAGINATOR_PER_PAGE));

        return $this->render(
            $isAjax ? 'leaderboard/_page-body.html.twig' : 'leaderboard/index.html.twig',
            [
                'leaderboard' => $query->getPage($page, $limit),
                'page' => $page,
                'limit' => $limit,
            ]
        );
    }

    #[Route(path: '/')]
    public function indexNoLocale(): Response
    {
        return $this->redirectToRoute('homepage', ['_locale' => $this->getParameter('locale')]);
    }
}
