<?php

declare(strict_types=1);

namespace App\Controller;

use App\Query\UserSearchQueryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class UserSearchController extends AbstractController
{
    #[Route(path: '/{_locale<%app.supported_locales%>}/user/search', name: 'user_search')]
    public function index(Request $request, UserSearchQueryInterface $query): Response
    {
        $isAjax = $request->isXmlHttpRequest() || (int)$request->query->get('ajax') === 1;
        $search = $request->query->get('q', '');

        return $this->render(
            $isAjax ? 'leaderboard/_page-body.html.twig' : 'user_search/index.html.twig',
            [
                'q' => $search,
                'users' => $query->search($search),
            ]
        );
    }
}
