<?php

declare(strict_types=1);

namespace App\Controller;

use App\Query\Sql\WordsQuery;
use App\Query\WordsQueryInterface;
use App\Repository\DictionaryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DictionaryController extends AbstractController
{
    private const VIEW_LIST = 'list';
    private const VIEW_DEF = 'def';

    #[Route(path: '/{_locale<%app.supported_locales%>}/dictionary', name: 'dictionary')]
    public function index(Request $request, WordsQuery $query): Response
    {
        $language = $request->query->get('lang', $request->attributes->get('_locale', $this->getParameter('locale')));
        $page = max(1, $request->query->getInt('page', 1));
        $view = $request->query->get('view', self::VIEW_DEF);
        $limit = max(1, $request->query->getInt('limit', WordsQueryInterface::PAGE_LIMIT));
        $search = $request->query->get('q');
        $isAjax = $request->isXmlHttpRequest() || (int)$request->query->get('ajax') === 1;

        return $this->render(
            $isAjax ? 'dictionary/_page-body.html.twig' : 'dictionary/index.html.twig',
            [
                'words' => $query->getPage($language, $page, $limit, $search),
                'totalCount' => $query->count($language),
                'page' => $page,
                'limit' => $limit,
                'language' => $language,
                'search' => $search,
                'offset' => ($page - 1) * $limit,
                'view' => in_array($view, [self::VIEW_LIST, self::VIEW_DEF], true) ? $view : self::VIEW_DEF,
            ]
        );
    }

    #[Route(path: '/dictionary', name: 'dictionary_no_locale')]
    public function dictionaryNoLocale(): Response
    {
        return $this->redirectToRoute('user_profile', ['_locale' => $this->getParameter('locale')]);
    }

    #[Route(path: '/{_locale<%app.supported_locales%>}/dictionary/show/{word}', name: 'show_word')]
    public function show(string $word, DictionaryRepository $repository): Response
    {
        return $this->render(
            'dictionary/show.html.twig',
            [
                'word' => $repository->findOneBy(['word' => $word]),
            ]
        );
    }
}
