<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class UserProfileController extends AbstractController
{
    #[Route(path: '{_locale<%app.supported_locales%>}/profile/{login}', name: 'user_profile')]
    public function index(mixed $login, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['login' => $login]);

        return $this->render(
            'user_profile/index.html.twig',
            [
                'controller_name' => 'UserProfileController',
                'user' => $user,
            ]
        );
    }

    #[Route(path: '/profile/{login}')]
    public function profileNoLocale(mixed $login): Response
    {
        return $this->redirectToRoute('user_profile', ['_locale' => $this->getParameter('locale'), 'login' => $login]);
    }
}
