<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\EasyCodingStandard\ValueObject\Option;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();
    $services->set(ArraySyntaxFixer::class)
        ->call(
            'configure',
            [
                [
                    'syntax' => 'short',
                ],
            ]
        );

    // Object Calisthenics rules. See: https://github.com/object-calisthenics/phpcs-calisthenics-rules

    $parameters = $containerConfigurator->parameters();
    $parameters->set(
        Option::PATHS,
        [
            __DIR__ . '/src',
            __DIR__ . '/shared',
        ]
    );

    $parameters->set(
        Option::SETS,
        [
            SetList::ARRAY,
            SetList::DOCBLOCK,
            SetList::NAMESPACES,
            SetList::CLEAN_CODE,
        ]
    );

    $parameters->set(
        Option::SKIP,
        [
            __DIR__ . '/../../src/Migrations',
        ]
    );
};
