<?php

namespace App\Tests\Unit\Twig;

use App\Twig\AppExtension;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Twig\Extension\ExtensionInterface;

final class AppExtensionTest extends TestCase
{
    private AppExtension $extension;

    protected function setUp(): void
    {
        parent::setUp();
        $this->extension = new AppExtension();
    }

    /**
     * @test
     */
    public function itShouldImplementExtensionInterface(): void
    {
        $this->assertInstanceOf(ExtensionInterface::class, $this->extension);
    }

    /**
     * @test
     */
    public function itShouldHighlightPassedTextWithDefaultClass(): void
    {
        $result = $this->extension->highlightFilter('some test string', ['test']);
        $this->assertEquals('some <span class="bg-warning">test</span> string', $result);
    }

    /**
     * @test
     */
    public function itShouldHighlightPassedTextWithCustomClass(): void
    {
        $result = $this->extension->highlightFilter('some test string', ['test'], 'search');
        $this->assertEquals('some <span class="search">test</span> string', $result);
    }

    /**
     * @dataProvider timeAgoDataProvider
     */
    public function testTimeAgoFilter(int|string|DateTimeInterface $time, string $expected): void
    {
        $this->assertEquals($expected, $this->extension->timeAgoFilter($time));
    }

    /**
     * @group dd
     */
    public function testGetFunctions(): void
    {
        $this->assertIsArray($this->extension->getFunctions());
    }

    public function testGetFilters(): void
    {
        $this->assertIsArray($this->extension->getFilters());
    }

    public function timeAgoDataProvider(): iterable
    {
//        yield [time(), '-'];
//        yield [time() - 1, 'a few seconds ago'];
        yield [time() - 59, 'a few seconds ago'];
        yield ['-1 seconds', 'a few seconds ago'];
        yield ['-50 seconds', 'a few seconds ago'];

        yield [time() - 60, 'a minute ago'];
        yield [time() - 120, '2 minutes ago'];
        yield ['-1 minutes', 'a minute ago'];
        yield ['-2 minutes', '2 minutes ago'];

        yield [time() - 3600, 'a hour ago'];
        yield [time() - 7200, '2 hours ago'];
        yield ['-1 hours', 'a hour ago'];
        yield ['-2 hours', '2 hours ago'];

        yield [time() - 86400, 'a day ago'];
        yield [time() - 86400 * 2, '2 days ago'];
        yield ['-1 days', 'a day ago'];
        yield ['-2 days', '2 days ago'];

        yield [time() - 86400 * 7, 'a week ago'];
        yield [time() - 86400 * 7 * 3, '3 weeks ago'];
        yield ['-1 weeks', 'a week ago'];
        yield ['-3 weeks', '3 weeks ago'];

        yield [time() - 86400 * 30, 'a month ago'];
        yield [time() - 86400 * 60, '2 months ago'];

        yield [time() - 86400 * 365, 'a year ago'];
        yield [time() - 86400 * 365 * 10, '10 years ago'];
        yield ['-1 year', 'a year ago'];
        yield ['-10 years', '10 years ago'];

        yield [new DateTimeImmutable('-1 year'), 'a year ago'];
        yield [new DateTimeImmutable('-10 years'), '10 years ago'];

        yield [new DateTime('-1 year'), 'a year ago'];
        yield [new DateTime('-10 years'), '10 years ago'];
    }
}
