<?php

declare(strict_types=1);

namespace App\Tests\Support\Context;

use Behat\Behat\Context\Context;

final class FeaturesContext implements Context
{
    /**
     * @When I great :name
     */
    public function hello(string $name)
    {
        echo "Hello $name";
    }
}
