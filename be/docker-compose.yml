version: '3'
networks:
    words-tier:
        driver: bridge
    stonehenge-network:
        external: true

services:
    web:
        build:
            context: .
            dockerfile: etc/docker/nginx/Dockerfile
        restart: always
        networks:
            - words-tier
            - stonehenge-network
        expose:
            - "8080"
            - "8989"
            - "8484"
            - "443"
            - "80"
        container_name: ${PROJECT_NAME:-words}_web_${CI_PIPELINE_ID:-1}
        extra_hosts:
            - words.local:127.0.0.1
        labels:
            - "traefik.enable=true"
            - "traefik.http.routers.${PROJECT_NAME}-app.entrypoints=https"
            - "traefik.http.routers.${PROJECT_NAME}-app.rule=Host(`${APP_HOST}`)"
            - "traefik.http.routers.${PROJECT_NAME}-app.tls=true"
#            - "traefik.http.services.${PROJECT_NAME}-app.loadbalancer.server.port=8080"
            - "traefik.http.services.${PROJECT_NAME}-app.loadbalancer.server.port=80"
            - "traefik.docker.network=stonehenge-network"

    php:
        build:
            context: .
            dockerfile: etc/docker/php8.1/Dockerfile
        container_name: ${PROJECT_NAME:-words}_php_${CI_PIPELINE_ID:-1}
        image: ${PROJECT_NAME:-words}_php:${PHP_TAG_ID:-1}
        working_dir: "/var/www/html"
        restart: on-failure
        environment:
            XDG_CACHE_HOME: "/var/www/var/cache"
            XDG_DATA_DIR: "/var/www/var/cache"
            PHP_IDE_CONFIG: "serverName=words"
        networks:
            - words-tier
        dns:
            - '8.8.8.8'
            - '8.8.4.4'

    mysql:
        build: etc/docker/mysql
        container_name: ${PROJECT_NAME:-words}_mysql_${CI_PIPELINE_ID:-1}
        restart: always
        command: --default-authentication-plugin=mysql_native_password
        env_file:
            - ".env.dev"
        environment:
            - MYSQL_DATABASE=${DATABASE_NAME}
            - MYSQL_ROOT_PASSWORD=${DATABASE_PASSWORD}
            - MYSQL_USER=${DATABASE_USER}
            - MYSQL_PASSWORD=${DATABASE_PASSWORD}
        networks:
            - words-tier
        entrypoint:
            sh -c "
            echo 'CREATE DATABASE IF NOT EXISTS ${DATABASE_NAME};' > /docker-entrypoint-initdb.d/init.sql;
            echo 'CREATE DATABASE IF NOT EXISTS ${DATABASE_NAME}_test;' > /docker-entrypoint-initdb.d/init.sql;
            /usr/local/bin/docker-entrypoint.sh --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci --default-authentication-plugin=mysql_native_password"

    redis:
        image: redis:5-alpine
        container_name: ${PROJECT_NAME:-words}_redis_${CI_PIPELINE_ID:-1}
        networks:
            - words-tier

    # Selenium
    browser:
        container_name: ${PROJECT_NAME:-pf}_browser_${CI_PIPELINE_ID:-1}
        hostname: browser
        # Pick/uncomment one
        # Pin selenium image to an older version
        # See https://github.com/docksal/docksal/issues/1096#issuecomment-543316840
        #    image: ${SELENIUM_IMAGE:-selenium/standalone-chrome-debug:3.141.59}
        image: ${SELENIUM_IMAGE:-selenium/standalone-chrome:4.1.0-20211123}
        #image: ${SELENIUM_IMAGE:-selenium/standalone-firefox-debug:3.141.59}
        volumes:
            # Workaround to avoid the browser crashing inside a docker container
            # See https://github.com/SeleniumHQ/docker-selenium#quick-start
            - /dev/shm:/dev/shm
        # VNC port for debugging
        # Host: <your-docker-host>:5900 (e.g, localhost:5900, 192.168.64.100:5900)
        # Password: secret
        dns:
            - '8.8.8.8'
            - '8.8.4.4'
        networks:
            - words-tier

