Основные требования:
* Реализовать в отдельной ветке feat/<DATE>
* Добавить поле viewCount  в сущность Dictionary
* Увеличивать viewCount на единицу при каждом просмотре страницы https://<HOST>/en/dictionary/show/{word}
* Отображать значение счетчика в формате "<Word> [<Number>]" на странице слова и в списке
* Добавить поле вывод счетчика в админку

Дополнительно:
* Добавить кнопку популярные и выводить слова с просмотрами от большего к меньшему
