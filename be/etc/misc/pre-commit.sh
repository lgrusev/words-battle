#!/usr/bin/env bash
# verify branch name
LC_ALL=C
if [ -z "$BRANCHES_TO_SKIP" ]; then
  BRANCHES_TO_SKIP=(master develop release)
fi

LOCAL_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
BRANCH_EXCLUDED=$(printf "%s\n" "${BRANCHES_TO_SKIP[@]}" | grep -c "^$LOCAL_BRANCH")
if [[ $BRANCH_EXCLUDED -eq 1 ]]; then
  exit 0
fi

if [[ ! "$LOCAL_BRANCH" =~ ^(release|Release).*$ ]]; then
    VALID_BRANCH_REGEX="^(feat|fix|docs|refactor|perf|test|chore|support)\/(SB|BON|SAG|DEVOPS|NP|SPT)-[0-9]+(_[a-zA-Z0-9_\-]*)?$"
    EXAMPLE="feat/SB-9999"
else
    VALID_BRANCH_REGEX="^Release\/v\.[0-9]{4}\.[0-9]+\.[0-9]+$"
    EXAMPLE="Release/v2021.89.1"
fi

MESSAGE="There is something wrong with your branch name.\n\
Branch names in this project must adhere to this contract: $VALID_BRANCH_REGEX.\n\
Your commit will be rejected.\n\
You should rename your branch to a valid name and try again (Example: $EXAMPLE)."

if [[ ! $LOCAL_BRANCH =~ $VALID_BRANCH_REGEX ]]
then
    echo -e "$MESSAGE"
    exit 1
fi


NO_TTY=1 make check-modified
rc=$?
if [[ $rc != 0 ]] ; then
    echo -e "\033[41m"Fix the error before commit."\033[0m"
    exit $rc;
fi

exit $rc
