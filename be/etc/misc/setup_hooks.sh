#!/bin/bash

# Installing pre-commit hook
if [ -f .git/hooks/pre-commit ]; then
    mv .git/hooks/pre-commit .git/hooks/pre-commit.old
fi
cp etc/misc/pre-commit.sh .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit


# Installing prepare-message-commit hook
#if [ -f .git/hooks/prepare-commit-msg ]; then
#    mv .git/hooks/prepare-commit-msg .git/hooks/prepare-commit-msg.old
#fi

#cp etc/misc/prepare-commit-msg.sh .git/hooks/prepare-commit-msg
#chmod +x .git/hooks/prepare-commit-msg
