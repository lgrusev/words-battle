// src/controllers/slideshow_controller.js
import {Controller} from "@hotwired/stimulus"

export default class extends Controller {
    static targets = ["container", "inputSearch"]
    static values = {
        page: {type: Number, default: 1},
        limit: {type: Number, default: 10},
        autoScroll: {type: Boolean, default: true},
        q: {type: String, default: ''},
        url: String
    }

    reset() {
        this.pageValue = 1;
        this.limitValue = 10;
        this.qValue = '';
        this.autoScrollValue = true;
    }

    getNext() {
        this.pageValue++
        this.load()
    }

    load() {
        const afterLoadCallback = this.autoScrollValue ? () => this.containerTarget.scrollIntoView({behavior: 'smooth', block: 'end'}) : () => {}
        fetch(this.url)
            .then(response => response.text())
            .then(html => $(this.containerTarget).append(html))
            .then(afterLoadCallback)
    }

    search() {
        this.qValue = this.inputSearchTarget.value;
        this.pageValue = 1;

        fetch(this.url)
            .then(response => response.text())
            .then(html => $(this.containerTarget).html(html))
    }

    get url() {
        const base_url = (this.urlValue || window.location.href)
        return (base_url.includes('?') ? base_url + '&' : base_url + '?') + this.params
    }

    get params() {
        return 'ajax=1&' + [this.page, this.limit, this.q].join("&");
    }

    get page() {
        return `page=${this.pageValue}`;
    }

    get limit() {
        return `limit=${this.limitValue}`;
    }

    get q() {
        return `q=${this.qValue}`;
    }
}