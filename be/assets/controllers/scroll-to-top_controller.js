import {Controller} from "@hotwired/stimulus"

/**
 * Usage example:
 *
 * <footer class="bg-dark py-4 mt-auto" {{ stimulus_controller("scroll") }}>
 *     <button type="button"
 *             {{ stimulus_action('scroll', 'scrollTop') }}
 *             {{ stimulus_target('scroll', 'btn') }}
 *             class="btn btn-danger btn-floating btn-lg rounded-circle"
 *             id="btn-back-to-top">
 *         <i class="bi bi-arrow-up"
 *         ></i>
 *     </button>
 * </footer>
 */
export default class extends Controller {
    static targets = ["btn"]

    initialize() {
        self = this
        window.onscroll = function () {
           self.scrollFunction()
        }
    }

    scrollTop() {
        window.scrollTo({top: 0, behavior: 'smooth'})
    }

    scrollFunction() {
        if (
            document.body.scrollTop > 20 ||
            document.documentElement.scrollTop > 20
        ) {
            this.btnTarget.style.display = "block";
        } else {
            this.btnTarget.style.display = "none";
        }
    }
}
